import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas

//document.querySelector('.pizzaListLink').getAttribute('class') += " active";
document
	.querySelector('.pizzaListLink')
	.setAttribute(
		'class',
		document.querySelector('.pizzaListLink').getAttribute('class') + ' active'
	);
